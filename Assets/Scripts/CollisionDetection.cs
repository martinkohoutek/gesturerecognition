﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PresentiGO.Gestures;

public class CollisionDetection : MonoBehaviour
{
    // Detecting gestures flag
    //bool m_DetectingGestures;

    // "Detecting gestures" text object
    public Text IsDetectingGesturesText;

    Gestures m_Gestures;

    // Use this for initialization
    void Start()
    {
        // Not detecting gestures at start
        //m_DetectingGestures = false;
        // Hide "Detecting gestures" text
        IsDetectingGesturesText.gameObject.SetActive( false );

        // Get gestures component
        m_Gestures = GameObject.Find( "DebugColliders" ).GetComponent<Gestures>();
    }
	
	// Update is called once per frame
	void Update()
    {
    }

    // Entered collision zone
    void OnTriggerEnter( Collider collider )
    {
        // Hand entered screen gesture zone
        if( gameObject.name == "ScreenGestureZone" && 
            collider.gameObject.name == "HandCollider" )
        {
            // Show "Detecting gestures" text
            ShowGestureDetectionUI();
            // Set detecting swipe flag
            m_Gestures.SetDetectingGestures( true );
            
            Debug.Log( "Entered gesture tracking zone" );
        }
        // Finger entered screen interaction zone 
        // TODO: restricted to index finger only
        else if( gameObject.name == "ScreenInteractionZone" && 
            collider.gameObject.tag == "FingerTip" )
        {
            // Set finger position on entering collision zone
            //m_Gestures.FingerGestureStart( collider.transform.position, collider.gameObject.name );
            m_Gestures.FingerGestureStart( collider );

            Debug.Log( "Entered touch screen zone" );
        }
    }

    void OnTriggerExit( Collider collider )
    {
        // Hand left screen gesture zone
        if( gameObject.name == "ScreenGestureZone" && 
            collider.gameObject.name == "HandCollider" )
        {
            // Hide "Detecting gestures" text
            HideGestureDetectionUI();
            // Clear detecting swipe flag
            m_Gestures.SetDetectingGestures( false );
            
            Debug.Log( "Left gesture tracking zone" );
        }
        // Finger left screen interaction zone
        // TODO: restricted to index finger only
        else if( gameObject.name == "ScreenInteractionZone" && 
            collider.gameObject.tag == "FingerTip" )
        {
            // Set finger position on leaving collision zone
            //m_Gestures.FingerGestureEnd( collider.transform.position, collider.gameObject.name );
            m_Gestures.FingerGestureEnd( collider );

            Debug.Log( "Left touch screen zone" );
        }
    }

    // Show "Detecting gestures" text
    public void ShowGestureDetectionUI()
    {
        IsDetectingGesturesText.gameObject.SetActive( true );
    }

    // Hide "Detecting gestures" text
    public void HideGestureDetectionUI()
    {
        //m_DetectingGestures = false;
        IsDetectingGesturesText.gameObject.SetActive( false );
    }
}
