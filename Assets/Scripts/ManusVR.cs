﻿using System;

namespace PresentiGO.VR.ManusGlovesVR
{
    using ManusVR;
    using UnityEngine;

    public class ManusVRInterface : MonoBehaviour
    {
        // Init session variable
        private static IntPtr m_Session;

        // Saving the data retrieved from the hand
        private static manus_hand_t m_LeftHand;
        private static manus_hand_t m_RightHand;
        private static HandValue[] m_HandValues = new HandValue[2];

        private static Vector3 m_InitialHandScale;

        // Init function
        // TODO: Already done in HandData. Get rid of useless stuff
        public static void ManusVRInit()
        {
            m_Session = HandData.GetSession();
            // TODO: remove? Manus.ManusSetCoordinateSystem( m_Session, coor_up_t.COOR_Y_UP, coor_handed_t.COOR_LEFT_HANDED );

            //m_Instance = HandData.GetInstance();
            m_HandValues[0].CloseValue = CloseValue.Open;
            m_HandValues[1].CloseValue = CloseValue.Open;

            Manus.ManusGetHand( m_Session, (device_type_t)0, out m_LeftHand );
            Manus.ManusGetHand( m_Session, (device_type_t)1, out m_RightHand );

            m_InitialHandScale = GameObject.Find( "lowerarm_l" ).transform.localScale;
        }

        // Calibrate hand size by touching the center of steering wheel 
        // by right hand index finger
        public static void CalibrateHandSize()
        {
            // Set default scale before every rescaling (to avoid increasing 
            //  of the gap between over and underscaling )
            SetDefaultHandSize();

            // Get tracker position
            //Vector3 trackerPosition = GameObject.Find( "Controller (right)/Model" ).transform.position;
            // Get elbow position
            Vector3 elbowPosition = GameObject.Find( "lowerarm_r" ).transform.position;
            // Get steering wheel and index finger tip positions and get their distance from elbow
            float elbowToWheelDist = ( GameObject.Find( "SteeringWheelLogo" ).transform.position - elbowPosition ).magnitude;
            float elbowToFingerTipDist = ( GameObject.Find( "index_04_r" ).transform.position - elbowPosition ).magnitude;

            /* Debug */
            //Debug.DrawLine( GameObject.Find( "SteeringWheelLogo" ).transform.position, elbowPosition, Color.green, 5f );
            //Debug.DrawLine( GameObject.Find( "index_04_r" ).transform.position, elbowPosition, Color.red, 5f );
            //Debug.Log( "wheel " + elbowToWheelDist );
            //Debug.Log( "finger " + elbowToFingerTipDist );

            // Calculate scale factor
            float scaleFactor = elbowToWheelDist / elbowToFingerTipDist;
            // Rescale hand from elbow
            GameObject.Find( "lowerarm_l" ).transform.localScale = m_InitialHandScale * scaleFactor;
            GameObject.Find( "lowerarm_r" ).transform.localScale = m_InitialHandScale * scaleFactor;
        }

        // Set default hand size
        public static void SetDefaultHandSize()
        {
            // Rescale hand from elbow
            GameObject.Find( "lowerarm_l" ).transform.localScale = m_InitialHandScale;
            GameObject.Find( "lowerarm_r" ).transform.localScale = m_InitialHandScale;
        }

        // Get hand data
        public static manus_ret_t GetHand( device_type_t handID, out manus_hand_t hand )
        {
            return Manus.ManusGetHand( m_Session, handID, out hand );
        }

        public static float GetWristRotation( manus_hand_t hand )
        {
            //Debug.Log( ( (Quaternion)hand.wrist ).eulerAngles.y );
            return ( (Quaternion)hand.wrist ).eulerAngles.y;
        }

        // Get average finger value
        public static double AverageFingerValue( manus_hand_t hand )
        {
            int sensors = 0;
            double total = 0;
            // Loop through all of the finger values
            for( int bendPosition = 0; bendPosition < 10; bendPosition++ )
            {
                // Only get the sensor values of the first bending point without the thumb (1,3,5,7)
                if( bendPosition % 2 != 0 && bendPosition < 8 )
                {
                    sensors++;
                    total += hand.raw.finger_sensor[bendPosition];
                }
            }
            return total / sensors;
        }

        // Update glove state
        public static void UpdateCloseValue( double averageSensorValue, device_type_t deviceType )
        {
            var values = Enum.GetValues( typeof( CloseValue ) );
            HandValue handValue;

            if( deviceType == device_type_t.GLOVE_LEFT )
                handValue = m_HandValues[0];
            else
                handValue = m_HandValues[1];

            CloseValue closest = CloseValue.Open;
            // Save the old value for comparisment
            CloseValue oldClose = handValue.CloseValue;

            // Get the current close value
            foreach( CloseValue item in values )
            {
                // Div by 100.0 is used because an enum can only contain ints
                if( averageSensorValue > (double)item / 100.0 )
                    closest = item;
            }
            handValue.CloseValue = closest;

            // Check if the hand just closed
            handValue.HandClosed = ( oldClose == CloseValue.Small && handValue.CloseValue == CloseValue.Fist );

            // Check if the hand just opened
            handValue.HandOpened = ( oldClose == CloseValue.Small && handValue.CloseValue == CloseValue.Open );

            if( deviceType == device_type_t.GLOVE_LEFT )
                m_HandValues[0] = handValue;
            else
                m_HandValues[1] = handValue;
        }
    }
}
