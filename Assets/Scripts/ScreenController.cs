﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenController : MonoBehaviour
{
    private int m_SelectedItem = 4;
    private GameObject[] m_Quads = new GameObject[9];

	// Use this for initialization
	void Start()
    {
        int i = 0;
        foreach( Transform child in GameObject.Find( "ScreenItems" ).transform )
        {
            m_Quads[i++] = child.gameObject;
        }
	}

	// Update is called once per frame
	void Update()
    {
	}

    public void SelectPrevious()
    {
        if( m_SelectedItem > 0 )
        {
            m_Quads[m_SelectedItem--].transform.localScale = new Vector3( 0.015f, 0.015f, 0.015f );
            m_Quads[m_SelectedItem].transform.localScale = new Vector3( 0.03f, 0.03f, 0.03f );
        }
    }

    public void SelectNext()
    {
        if( m_SelectedItem < 8 )
        {
            m_Quads[m_SelectedItem++].transform.localScale = new Vector3( 0.015f, 0.015f, 0.015f );
            m_Quads[m_SelectedItem].transform.localScale = new Vector3( 0.03f, 0.03f, 0.03f );
        }
    }
}
