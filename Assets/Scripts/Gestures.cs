﻿using System;
using UnityEngine;
using ManusVR;
using PresentiGO.VR.ManusGlovesVR;
using UnityEngine.UI;

namespace PresentiGO.Gestures
{
    class Gestures : MonoBehaviour
    {
        // Saving the data retrieved from the hand
        private manus_hand_t m_LeftHand;
        private manus_hand_t m_RightHand;

        // Gesture tracking
        private bool m_DetectingGestures = false;
        private float[] m_RightWristRotations = new float[10];
        private int m_RightWristRotationsCount = 0;
        
        // Finger tracking
        private const float m_DragDropRange = 0.5f;
        private bool m_DetectingFinger = false;
        //private Vector3 m_FingerPositionStart;
        //private Vector3 m_FingerPositionEnd;
        private string m_DetectedFingerName;
        private ScrollRect m_ScrollRect;
        private Transform m_FingerPosition;
        private float m_TouchHorizontalValue;
        private float m_TouchVerticalValue;
        private float m_SBHorizontalValue;
        private float m_SBVerticalValue;

        // Debug ray length 
        private const float m_DebugRayLength = 0.1f;

        // Old gesture tracking
        public double HandOpenValue = 0.09;
        public double HandClosedValue = 0.15;
        public float SwipeDuration = 0.1f;
        public float SwipePauseDuration = 0.5f;
        private bool m_SwipeTracking = false;
        private float m_SwipeTimer = 0.5f;
        private CloseValue m_SwipeStartPose = CloseValue.Fist;


        // ScreenController for gestures actions
        ScreenController m_ScreenController;
        
        // Start function
        public void Start()
        {
            Init();
        }

        // Initialization
        public void Init()
        {
            // ManusVR initialization
            ManusVRInterface.ManusVRInit();
            
            m_ScreenController = GameObject.Find( "ScreenItems" ).GetComponent<ScreenController>();

            m_ScrollRect = GameObject.Find( "Scroll View" ).GetComponent<ScrollRect>();
        }

        // Update function
        public void Update()
        {
            // Detecting finger-screen interaction (drag'n'drop). Priority over 
            // gestures detection
            if( m_DetectingFinger )
            {
                // TODO: Get constants from screen boundaries
                // Set scrollRect h/v values
                m_ScrollRect.horizontalScrollbar.value = m_SBHorizontalValue + ( m_TouchHorizontalValue - GetScreenHPos( m_FingerPosition.position.x )  ) * m_DragDropRange;
                m_ScrollRect.verticalScrollbar.value = m_SBVerticalValue + ( m_TouchVerticalValue - GetScreenVPos( m_FingerPosition.position.y ) ) * m_DragDropRange;
            }
            // Detecting hand gestures
            else if( m_DetectingGestures )
            {
                DetectGestures();
            }

            // Calibrate hands size. Touch steering wheel center with right hand
            // index finger. Finger tip, wrist, and tracker should be in line
            if( Input.GetKeyUp( KeyCode.C ) )
            {
                ManusVRInterface.CalibrateHandSize();
                Debug.Log( "Hands resized" );
            }
            else if( Input.GetKeyUp( KeyCode.V ) )
            {
                ManusVRInterface.SetDefaultHandSize();
                Debug.Log( "Set default hands size" );
            }
            // Setup vehicle position and rotation
            else if( Input.GetKeyUp( KeyCode.X ) )
            {
                // Set head object name
                string head = "Camera (head)";//"Head"
                // Get vehicle transform
                Transform vehicleTransform = GameObject.Find( "Vehicle" ).transform;
                // Get head transform
                Transform headTransform = GameObject.Find( head ).transform;
                // Get head y-rotation
                float headYRot = headTransform.rotation.eulerAngles.y;
                // Get vehicle y-rotation
                float vehicleYRot = vehicleTransform.rotation.eulerAngles.y;
                // Calculate head-vehicle y-rotation difference
                float rot = headYRot - vehicleYRot - 90f;
                // Rotate vehicle around head along y-axis by diff angle
                vehicleTransform.RotateAround( headTransform.position, Vector3.up, rot );
            }
        }

        // Detect gestures. Called every frame
        public void DetectGestures()
        {
            //manus_hand_t leftHand;
            manus_hand_t rightHand;
            
            // Get right hand data
            if( ManusVRInterface.GetHand( device_type_t.GLOVE_RIGHT, out rightHand ) == manus_ret_t.MANUS_SUCCESS )
            {
                m_RightHand = rightHand;
                // Get average finger value
                double averageFingerValue = ManusVRInterface.AverageFingerValue( m_RightHand );
                // Update hand state
                ManusVRInterface.UpdateCloseValue( averageFingerValue, device_type_t.GLOVE_RIGHT );

                // Detect swipe gesture by wrist rotation
                GestureSwipeDetection();
                
                #region Obsolete gesture detection
                #if false
                // Check if detecting swiping by gesture. Flag is set when entering screen gesture-detection space
                if( m_DetectingGestureSwipe )
                {
                    GestureSwipeDetection( averageFingerValue );
                }
                #endif
                #endregion
            }
        }

        #region Obsolete gesture detection
        #if false
        // Gesture swiping. Based on fingers rotation (hand state)
        private void GestureSwipeDetection( double averageFingerValue )
        {
            // Not tracking yet
            if( m_SwipeTracking == false )
            {
                // Wait for pause timer (time betwen gestures) to run out
                if( m_SwipeTimer > 0f )
                {
                    // Decrease timer
                    m_SwipeTimer -= Time.deltaTime;
                }
                // Wait timer elapsed
                else
                {
                    // Actual glove state - small fist
                    if( averageFingerValue > HandClosedValue )
                    {
                        // Set start pose variable
                        m_SwipeStartPose = CloseValue.Small;
                        // Start swipe tracking
                        m_SwipeTracking = true;
                        // Start timer
                        m_SwipeTimer = SwipeDuration;
                    }
                    // Actual glove state - open hand
                    else if( averageFingerValue < HandOpenValue )
                    {
                        // Set start pose variable
                        m_SwipeStartPose = CloseValue.Open;
                        // Start swipe tracking
                        m_SwipeTracking = true;
                        // Start timer
                        m_SwipeTimer = SwipeDuration;
                    }
                }
            }
            // Already tracking
            else
            {
                // Decrease timer
                m_SwipeTimer -= Time.deltaTime;

                // Tracking failed - timer elapsed before a gesture was detected
                if( m_SwipeTimer <= 0f )
                {
                    // Stop tracking swipe
                    StopTrackingGestureSwipe();
                }

                // Actual glove state - small fist and start pose was open -> swiped
                if( averageFingerValue > HandClosedValue && m_SwipeStartPose == CloseValue.Open )
                {
                    // Swiped left
                    ScreenTextMesh.text = "Gesture swiped left";
                    Debug.Log( "Gesture swiped left" );

                    // Stop tracking swipe
                    StopTrackingGestureSwipe();
                    // Set pause timer before another gesture
                    m_SwipeTimer = SwipePauseDuration;
                }
                // Actual glove state - open hand and start pose was small fist -> swiped
                else if( averageFingerValue < HandOpenValue && m_SwipeStartPose == CloseValue.Small )
                {
                    // Swiped right
                    ScreenTextMesh.text = "Gesture swiped right";
                    Debug.Log( "Gesture swiped right" );

                    // Stop tracking swipe
                    StopTrackingGestureSwipe();
                    // Set pause timer before another gesture
                    m_SwipeTimer = SwipePauseDuration;
                }
            }
        }
        #endif
        #endregion

        // Gesture swiping. Based on wrist rotation
        private void GestureSwipeDetection()
        {
            // If array is fully populated, detect gesture swiping
            if( m_RightWristRotationsCount == 10 )
            {
                // Get wrist rotation along y-axis
                float wristRotation = ManusVRInterface.GetWristRotation( m_RightHand );
                // If rotation value is below 50, add 360 (avoiding fake swipes)
                if( wristRotation < 50f )
                {
                    wristRotation += 360f;
                }

                // Push new wrist value into array of values
                for( int i = 9; i > 0; i-- )
                {
                    m_RightWristRotations[i - 1] = m_RightWristRotations[i];
                }
                m_RightWristRotations[m_RightWristRotationsCount - 1] = wristRotation;

                // Seek for abs max difference between wrist rotation values
                float maxDiff = wristRotation - m_RightWristRotations[m_RightWristRotationsCount - 2];
                for( int i = 7; i >= 0; i-- )
                {
                    float d = wristRotation - m_RightWristRotations[i];
                    if( Math.Abs( d ) > Math.Abs( maxDiff ) )
                    {
                        maxDiff = d;
                    }
                }

                //float diff = maxDiff;// m_RightWristRotations[m_RightWristRotationsCount - 1] - m_RightWristRotations[0];

                // Diff is negative and over threshold. Swiped left 
                if( maxDiff < -120f /*&& diff > -250f*/ )
                {
                    // Swipe left
                    Debug.Log( "Swiped left" );
                    m_ScreenController.SelectPrevious();
                    // Populate wrist array with the last value (avoid fake swipe)
                    PopulateWristArray( m_RightWristRotations[m_RightWristRotationsCount - 1] );
                }
                // Diff is positive and over threshold. Swiped right
                else if( maxDiff > 120f /*&& diff < 250f*/ )
                {
                    // Swipe right
                    Debug.Log( "Swiped right" );
                    m_ScreenController.SelectNext();
                    // Populate wrist array with the last value (avoid fake swipe)
                    PopulateWristArray( m_RightWristRotations[m_RightWristRotationsCount - 1] );
                }
            }
            // Array is not fully populated. Push wrist rotation value
            else
            {
                m_RightWristRotations[m_RightWristRotationsCount++] = ManusVRInterface.GetWristRotation( m_RightHand );
            }
        }

        // Populate wrist array with given value
        private void PopulateWristArray( float value )
        {
            for( int i = 0; i < 9; i++ )
            {
                m_RightWristRotations[i] = value;
            }
        }

        // Get flag indicating if tracking gesture swiping
        public bool GetDetectingGestures()
        {
            return m_DetectingGestures;
        }

        // Set flag indicating if tracking gesture swiping
        public void SetDetectingGestures( bool detectingGestures )
        {
            m_DetectingGestures = detectingGestures;
        }

        #region Obsolete gesture detection
        #if false
        // Gesture swipe tracking has finished
        private void StopTrackingGestureSwipe()
        {
            // Stop tracking gesture swipe
            m_SwipeTracking = false;
            // Set default glove pose
            m_SwipeStartPose = CloseValue.Fist;
        }
        #endif
        #endregion

        // Finger gesture start function
        public void FingerGestureStart( Collider finger )// Vector3 position, string fingerName )
        {
            // Return if already detecting finger
            if( m_DetectingFinger )
            {
                return;
            }

            // Set flag that finger detection is active
            m_DetectingFinger = true;
            m_DetectedFingerName = finger.gameObject.name;// fingerName;

            // Set finger start position
            //m_FingerPositionStart = finger.transform.position;// position;

            // Get finger transform
            m_FingerPosition = finger.transform;

            // Save touched (h/v) scrollView values
            m_TouchHorizontalValue = GetScreenHPos( m_FingerPosition.position.x );
            m_TouchVerticalValue = GetScreenVPos( m_FingerPosition.position.y );

            // Save (h/v) scrollBars values
            m_SBHorizontalValue = m_ScrollRect.horizontalScrollbar.value;
            m_SBVerticalValue = m_ScrollRect.verticalScrollbar.value;
        }

        // Finger gesture end function
        public void FingerGestureEnd( Collider finger )// Vector3 position, string fingerName )
        {
            if( m_DetectedFingerName != finger.gameObject.name )// fingerName )
            {
                return;
            }
            // Clear flag that finger detection is active
            m_DetectingFinger = false;

            // Set finger end position
            //m_FingerPositionEnd = finger.transform.position;// position;

            // Detect finger gesture
            //DetectFingerGesture( zoneTransform );
        }

        // Recalculate world X position to screen position
        private float GetScreenHPos( float hPos )
        {
            return ( hPos - 0.2f ) * 3.3f;
        }
        // Recalculate world Y position to screen position
        private float GetScreenVPos( float vPos )
        {
            return ( vPos - 1.07f ) * 12.5f;
        }

        #region Obsolete finger gesture detection
#if false
        // Detect finger gesture function
        private void DetectFingerGesture( Transform zoneTransform )
        {
            // Get finger gesture vector
            Vector3 fingerSwipe = m_FingerPositionEnd - m_FingerPositionStart;
            // Get vector length
            float fingerSwipeLength = fingerSwipe.magnitude;

            // Touch. If not enough long for swipe
            if( fingerSwipeLength < 0.05f )
            {
                // Set gesture string for appropriate screen
                if( zoneTransform.name == "ScreenInteractionZone" )
                {
                    ScreenTextMesh.text = "Touched";
                }
                else if( zoneTransform.name == "CentralConsoleInteractionZone" )
                {
                    CentralConsoleTextMesh.text = "Touched";
                }
                Debug.Log( "Touched display at " + m_FingerPositionStart );

                // Create sphere for 3s at touched point
                GameObject touchPoint = GameObject.CreatePrimitive( PrimitiveType.Sphere );
                touchPoint.transform.position = m_FingerPositionStart;
                touchPoint.transform.localScale = new Vector3( 0.01f, 0.01f, 0.01f );
                Destroy( touchPoint, 3f );
            }
            // Swipe. Gesture vector is long enough for swipe
            else
            {
                string result = "";
                Debug.Log( "Swiped. But where? (Swipe length: " + fingerSwipeLength + ")" );
                fingerSwipe.Normalize();

                // Threshold for swipe angle difference 
                const float maxSwipeAngleDiff_ = 25f;

                // Get angle between swipe vector and zone right vector
                float angleHorizontal = Vector3.Angle( fingerSwipe, zoneTransform.right );

                // Call left swipe function if swipe vector is within angle threshold
                if( angleHorizontal < maxSwipeAngleDiff_ )
                {
                    FingerSwipeLeft( zoneTransform );
                    result = "Swiped left";
                }
                // Call right swipe function if swipe vector is within angle threshold
                else if( angleHorizontal > 180f - maxSwipeAngleDiff_ )
                {
                    FingerSwipeRight( zoneTransform );
                    result = "Swiped right";
                }
                else
                {
                    // Get angle between swipe vector and zone up vector
                    float angleVertical = Vector3.Angle( fingerSwipe, zoneTransform.up );

                    // Call up swipe function if swipe vector is within angle threshold
                    if( angleVertical < maxSwipeAngleDiff_ )
                    {
                        FingerSwipeUp( zoneTransform );
                        result = "Swiped up";
                    }
                    // Call down swipe function if swipe vector is within angle threshold
                    else if( angleVertical > 180f - maxSwipeAngleDiff_ )
                    {
                        FingerSwipeDown( zoneTransform );
                        result = "Swiped down";
                    }
                }

                // Set swipe result/direction for appropriate screen
                if( zoneTransform.name == "ScreenInteractionZone" )
                {
                    ScreenTextMesh.text = result;
                }
                else if( zoneTransform.name == "CentralConsoleInteractionZone" )
                {
                    CentralConsoleTextMesh.text = result;
                }
            }
        }

        // Finger left swipe function
        private void FingerSwipeLeft( Transform zoneTransform )
        {
            // Draw left swipe vector ray if within angle threshold
            Debug.DrawRay( zoneTransform.position, -zoneTransform.right * m_DebugRayLength, Color.blue, 2f );
        }

        // Finger right swipe function
        private void FingerSwipeRight( Transform zoneTransform )
        {
            // Draw right swipe vector ray if within angle threshold
            Debug.DrawRay( zoneTransform.position, zoneTransform.right * m_DebugRayLength, Color.green, 2f );
        }

        // Finger up swipe function
        private void FingerSwipeUp( Transform zoneTransform )
        {
            // Draw up swipe vector ray if within angle threshold
            Debug.DrawRay( zoneTransform.position, zoneTransform.up * m_DebugRayLength, Color.cyan, 2f );
        }

        // Finger down swipe function
        private void FingerSwipeDown( Transform zoneTransform )
        {
            // Draw down swipe vector ray if within angle threshold
            Debug.DrawRay( zoneTransform.position, -zoneTransform.up * m_DebugRayLength, Color.magenta, 2f );
        }
#endif
        #endregion
    }
}
