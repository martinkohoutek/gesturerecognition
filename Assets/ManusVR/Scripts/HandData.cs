﻿using UnityEngine;
using ManusVR;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public enum Finger
{
    thumb,
    index,
    middle,
    ring,
    pink
}

public enum FingerSensor
{
    pink = 1,
    ring = 3,
    middle = 5,
    index = 7,
    thumb = 9,
    unused = 10
}

/// <summary>
/// List of values/states to check for each hand
/// Go from big to small numbers
/// </summary>
public enum CloseValue
{
    Fist = 65,
    Small = 40,
    Open = 5
}

/// <summary>
/// State of each hand
/// </summary>
public struct HandValue
{
    public CloseValue CloseValue;
    public bool IsClosed;
    public bool IsOpen;
    public bool HandOpened;
    public bool HandClosed;
}

public class HandData : MonoBehaviour
{
    private static IntPtr session;

    // Saving the data retrieved from the hand
    private manus_hand_t _leftHand;
    private manus_hand_t _rightHand;

    // Save if the last data was retrieved correctly
    private manus_ret_t _leftRet;
    private manus_ret_t _rightRet;

    public static HandData Instance;
    
    /// <summary>
    /// Check if the given hand is closed
    /// </summary>
    /// <param name="deviceType"></param>
    /// <returns></returns>
    public bool IsClosed( device_type_t deviceType )
    {
        return _handValues[(int)deviceType].CloseValue == CloseValue.Fist;
    }
    /// <summary>
    /// Check if the given hand is open
    /// </summary>
    /// <param name="deviceType"></param>
    /// <returns></returns>
    public bool IsOpen( device_type_t deviceType )
    {
        return _handValues[(int)deviceType].CloseValue == CloseValue.Open;
    }
    /// <summary>
    /// Check if the hand just opened
    /// </summary>
    /// <param name="deviceType"></param>
    /// <returns></returns>
    public bool HandOpened( device_type_t deviceType )
    {
        return _handValues[(int)deviceType].HandOpened;
    }
    /// <summary>
    /// Check if the hand just closed
    /// </summary>
    /// <param name="deviceType"></param>
    /// <returns></returns>
    public bool HandClosed( device_type_t deviceType )
    {
        return _handValues[(int)deviceType].HandClosed;
    }
    private HandValue[] _handValues = new HandValue[2];

    // Get instance
    public static HandData GetInstance()
    {
        return Instance;
    }

    // Get instance
    public static IntPtr GetSession()
    {
        return session;
    }

    // Use this for initialization
    void Start()
    {
        Manus.ManusInit( out session );
        Manus.ManusSetCoordinateSystem( session, coor_up_t.COOR_Y_UP, coor_handed_t.COOR_LEFT_HANDED );

        if( Instance == null )
            Instance = this;
        _handValues[0].CloseValue = CloseValue.Open;
        _handValues[1].CloseValue = CloseValue.Open;

        Manus.ManusGetHand( session, (device_type_t)0, out _leftHand );
        Manus.ManusGetHand( session, (device_type_t)1, out _rightHand );
    }

    // Update is called once per frame
    void Update()
    {
        // TODO: Try to omit this code (useless update code here?)
        manus_hand_t leftHand;
        manus_hand_t rightHand;

        // if the retrieval of the handdata is succesfull update the local value and wether the hand is closed
        if( Manus.ManusGetHand( session, device_type_t.GLOVE_LEFT, out leftHand ) == manus_ret_t.MANUS_SUCCESS )
        {
            _leftHand = leftHand;
            double averageFingerValue = AverageFingerValue( _rightHand );
            UpdateCloseValue( averageFingerValue, device_type_t.GLOVE_LEFT );
        }

        if( Manus.ManusGetHand( session, device_type_t.GLOVE_RIGHT, out rightHand ) == manus_ret_t.MANUS_SUCCESS )
        {
            _rightHand = rightHand;
            double averageFingerValue = AverageFingerValue( _rightHand );
            UpdateCloseValue( averageFingerValue, device_type_t.GLOVE_RIGHT );
        }
    }

    /// <summary>
    /// Check if there is valid output for the given device
    /// </summary>
    /// <param name="deviceType"></param>
    /// <returns></returns>
    public bool ValidOutput( device_type_t deviceType )
    {
        if( deviceType == device_type_t.GLOVE_LEFT )
            return _leftRet == manus_ret_t.MANUS_SUCCESS;
        else
            return _rightRet == manus_ret_t.MANUS_SUCCESS;
    }

    /// <summary>
    /// Get the thumb imu rotation of the given device
    /// </summary>
    /// <param name="deviceType"></param>
    /// <returns></returns>
    public Quaternion GetImuRotation( device_type_t deviceType )
    {
        if( deviceType == device_type_t.GLOVE_LEFT )
            return _leftHand.raw.imu[1];
        else if( deviceType == device_type_t.GLOVE_RIGHT )
            return _rightHand.raw.imu[1];
        return Quaternion.identity;
    }

    /// <summary>
    /// Get the wrist rotation of a given device
    /// </summary>
    /// <param name="deviceType">The device type</param>
    /// <returns></returns>
    public Quaternion GetWristRotation( device_type_t deviceType )
    {
        if( deviceType == device_type_t.GLOVE_LEFT )
            return _leftHand.wrist;
        else if( deviceType == device_type_t.GLOVE_RIGHT )
            return _rightHand.wrist;
        else
            return Quaternion.identity;
    }

    /// <summary>
    /// Get the rotation of the given finger
    /// </summary>
    /// <param name="finger"></param>
    /// <param name="deviceType"></param>
    /// <param name="pose"></param>
    /// <returns></returns>
    public Quaternion GetFingerRotation( Finger finger, device_type_t deviceType, int pose )
    {
        manus_hand_t hand;
        if( deviceType == device_type_t.GLOVE_LEFT )
            hand = _leftHand;
        else
            hand = _rightHand;

        Quaternion fingerRotation = hand.fingers[(int)finger].joints[pose].rotation;

        if( fingerRotation == null )
            return Quaternion.identity;
        return fingerRotation;
    }


    /// <summary>
    /// Get the average value of all fingers combined on the given hand
    /// </summary>
    /// <param name="hand"></param>
    /// <returns></returns>
    private double AverageFingerValue( manus_hand_t hand )
    {
        int sensors = 0;
        double total = 0;
        // Loop through all of the finger values
        for( int bendPosition = 0; bendPosition < 10; bendPosition++ )
        {
            // Only get the sensor values of the first bending point without the thumb (1,3,5,7)
            if( bendPosition % 2 != 0 && bendPosition < 8 )
            {
                sensors++;
                total += hand.raw.finger_sensor[bendPosition];
            }
        }
        return total / sensors;
    }

    private void UpdateCloseValue( double averageSensorValue, device_type_t deviceType )
    {
        var values = Enum.GetValues( typeof( CloseValue ) );
        HandValue handValue;
        if( deviceType == device_type_t.GLOVE_LEFT )
            handValue = _handValues[0];
        else
            handValue = _handValues[1];

        CloseValue closest = CloseValue.Open;
        // Save the old value for comparisment
        CloseValue oldClose = handValue.CloseValue;

        // Get the current close value
        foreach( CloseValue item in values )
        {
            // Div by 100.0 is used because an enum can only contain ints
            if( averageSensorValue > (double)item / 100.0 )
                closest = item;
        }
        handValue.CloseValue = closest;

        // Check if the hand just closed
        handValue.HandClosed = ( oldClose == CloseValue.Small && handValue.CloseValue == CloseValue.Fist );/*oldClose < handValue.CloseValue;*/
        // Check if the hand just opened
        handValue.HandOpened = ( oldClose == CloseValue.Small && handValue.CloseValue == CloseValue.Open );

        if (deviceType == device_type_t.GLOVE_LEFT)
            _handValues[0] = handValue;
        else
            _handValues[1] = handValue;
    }

    private void DebugHand( HandValue handValue , device_type_t glove, double val )
    {
        if( handValue.HandClosed )
        {
            Debug.Log( glove + " just closed! (" + (int)val + ")" );
        }
        else //if( handValue.HandOpened )
        {
            Debug.Log( glove + " just opened! (" + (int)val + ")" );
        }
    }
}
